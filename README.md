# Flutter Test using Sonarqube


REQUIREMENTS
------------

This module requires the following modules:

You should already install supporting software like:
 * Flutter SDK (https://flutter.dev/docs/get-started/install)
 * SonarQube (https://www.sonarqube.org/downloads/) ,the latest sonarqube 
  still working on java 8 is version 7.8 ,the latest version using java 11, or using custom docker already
  have external plugins (https://hub.docker.com/r/drikalabs/custom-sonarqube-flutter)
 * Sonar Scanner (https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/)
 * Plugins SonarQube for flutter(https://github.com/insideapp-oss/sonar-flutter) 
 download (https://github.com/insideapp-oss/sonar-flutter/releases)
 * Ngrok (https://ngrok.com/download)

## Getting Started

- First you should configure Sonarqube after download and extract file, 
 after that copy plugins flutter to directory "$SONARQUBE/extensions/plugins" .
  after that run the Sonarqube. open you browser and go to (http://localhost:9000),
   default access for login is username admin ,password admin. or using my custom docker image and run it
   ```
   $ docker pull drikalabs/custom-sonarqube-flutter:0.0.1
   $ docker run  -p 9000:9000 drikalabs/custom-sonarqube-flutter:0.0.1
   ``` 
  make sure you already login to your docker-hub account
  NOTE: you can use Ngrok for netwoking 
      ```
      ./ngrok http 9000
      ``` that will be give you public address for accessing your sonarqube
 - Go to Sonarqube page , login and create project and you should have to generate token for your project  
 - Create a flutter project

   ```
   $ flutter create app_name
   ``` 
- Open Project Using your IDE , create file " sonar-project.properties " .

    ```
    project/
    └── lib/
    ├── test/
    ├── sonar-project.properties
  
    ```
 - Configure " sonar-project.properties "
     ```properties
    # Project identification
    sonar.projectKey=Flutter-Integration
    sonar.projectName=Flutter-Integration
    sonar.projectVersion=1.0
    	
    # Source code location.
    # Path is relative to the sonar-project.properties file. Defaults to .
    # Use commas to specify more than one folder.
    sonar.sources=lib
    sonar.tests=test
    
   # this is address from ngrok or also you can use http://localhost:9000 for local usage
    sonar.host.url=http://0f09299a.ngrok.io/
    
    # this token from creating project from sonarqube page 
    sonar.login=bd48b89c1093fbae49281afbaed5b92e11975322
    
    # Encoding of the source code. Default is default system encoding.
    sonar.sourceEncoding=UTF-8

    ```
  - Follow this command for execute sonarqube testing
      ```
    $ flutter pub get
    $ flutter clean
    $ flutter test
    $ flutter test --coverage
    $ flutter test --machine > tests.output 
    $ sonar-scanner
      ```
    this is important because the plugins actually just read result from result of test so 
    the data will be able to show in sonarqube project page .
    and this is most important command you should run ``` $ flutter test --machine > tests.output``` 
    ,this command will create some file and file will be read by sonar-scanner